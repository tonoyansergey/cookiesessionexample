package com.egs.repositories;

import com.egs.models.User;

import java.util.ArrayList;
import java.util.List;

public class Storage {

    private static Storage instance;
    private List<User> userList;


    public List<User> getUserList() {
        return userList;
    }

    private Storage () {
        userList = new ArrayList<>();
        userList.add(new User("jackie", "0000"));
        userList.add(new User("dog458", "1235"));
        userList.add(new User("melody99", "1284"));
    }

    public static Storage getInstance() {
        if (instance == null) {
            instance = new Storage();
        }

        return instance;
    }
}
