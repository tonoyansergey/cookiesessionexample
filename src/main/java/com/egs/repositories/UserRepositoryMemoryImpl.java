package com.egs.repositories;

import com.egs.models.User;

import java.util.List;

public class UserRepositoryMemoryImpl implements UserRepository{


    @Override
    public List<User> getUserList() {
        return Storage.getInstance().getUserList();
    }

    @Override
    public void addUser(User user) {
        Storage.getInstance().getUserList().add(user);
    }

    @Override
    public boolean exist(String userName, String password) {

        for (User user: Storage.getInstance().getUserList()) {
            if (user.getUserName().equals(userName) && user.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }
}
