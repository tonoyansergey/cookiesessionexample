package com.egs.repositories;

import com.egs.models.User;

import java.util.List;

public interface UserRepository {

    List<User> getUserList ();

    void addUser (User user);

    boolean exist (String userName, String password);
}
