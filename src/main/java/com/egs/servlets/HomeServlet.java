package com.egs.servlets;

import com.egs.repositories.UserRepository;
import com.egs.repositories.UserRepositoryMemoryImpl;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/")
public class HomeServlet extends HttpServlet {

    UserRepository userRepository;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("usersFromRep", userRepository.getUserList());
        req.getServletContext().getRequestDispatcher("/views/home.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String color = req.getParameter("color");

        Cookie colorCookie = new Cookie("color", color);
        resp.addCookie(colorCookie);

        resp.sendRedirect(req.getContextPath() + "/");
    }

    @Override
    public void init() throws ServletException {
        userRepository = new UserRepositoryMemoryImpl();
    }
}
