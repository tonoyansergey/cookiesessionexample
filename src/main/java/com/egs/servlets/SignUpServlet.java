package com.egs.servlets;

import com.egs.models.User;
import com.egs.repositories.UserRepository;
import com.egs.repositories.UserRepositoryMemoryImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/signUp")
public class SignUpServlet extends HttpServlet {

    UserRepository userRepository;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getServletContext().getRequestDispatcher("/views/signUp.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName = req.getParameter("login");
        String password = req.getParameter("password");

        if (userRepository.exist(userName, password)) {
            resp.sendRedirect(req.getContextPath() + "/signUp");
        } else {
            userRepository.addUser(new User(userName, password));
            resp.sendRedirect(req.getContextPath() + "/login");
        }
    }

    @Override
    public void init() throws ServletException {
        userRepository = new UserRepositoryMemoryImpl();
    }
}
