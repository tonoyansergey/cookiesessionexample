<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 4/28/2019
  Time: 12:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
<head>
    <title>Home</title>
</head>
<body>
        <table style="color: ${cookie.color.value}" border="1" align="center">
            <tr>
                <th>Registered users</th>
            </tr>
            <c:forEach items="${usersFromRep}" var="user">
                <tr>
                    <td>${user.userName}</td>
                </tr>
            </c:forEach>
        </table>
        <div align="center">
            <form method="post" action="/">
            <label for="color">
                <select name="color" id="color">
                    <option value="red">Red</option>
                    <option value="yellow">Yellow</option>
                    <option value="green">Green</option>
                </select>
            </label><br>
            <input type="submit" value="Choose text color">
            </form>
        </div>
</body>
</html>
