<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 4/28/2019
  Time: 13:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
    <h3>Log In to continue or <a href="signUp">Sign Up</a> </h3>
    <form method="post" action="login">
        <label for="login">Username
            <input type="text" id="login" name="login" required>
        </label><br>
        <label for="pass">Password
            <input type="password" id="pass" name="password" required><br>
        </label>
        <input type="submit" value="Log In">
    </form>

</body>
</html>
